extends RigidBody2D

var SizeX = 0.0;
var SizeY = 0.0;
var Sleep = 10;
var Stop = false;

func _ready():
	add_to_group("Blocks");
	set_fixed_process(true);
	
	SizeX = round(rand_range(1, 10)) * 32;
	SizeY = round(rand_range(1, 4)) * 32;
	
	get_node("Sprite").set_region_rect(Rect2(0, 0, SizeX, SizeY));
	get_node("Sprite").set_modulate(Color(rand_range(0.2, 0.9), rand_range(0.2, 0.9), rand_range(0.2, 0.9), 1));
	get_node("Sprite/Light2D").set_color(get_node("Sprite").get_modulate());
	get_node("Sprite/Light2D").set_texture_offset(Vector2((int(SizeX / 32.0) % 2 * 16), (int(SizeY / 32) % 2 * 16)));

	var shape = RectangleShape2D.new();
	shape.set_extents(Vector2(SizeX / 2, SizeY / 2));
	add_shape(shape);
	
	set_mass((SizeX / 64) * (SizeY / 64) * 4);
	set_angular_velocity(rand_range(-10, 10.0));

func _fixed_process(delta):
	Sleep -= delta;
	if (Sleep < 0):
		set_can_sleep(true);
	if (get_pos().y < 360 && get_linear_velocity().y < 0.05 && get_linear_velocity().y > -0.05 && Sleep < 5 && Stop == false):
		var blocks = get_tree().get_nodes_in_group("Blocks");
		for i in blocks:
			i.get_node("Anim").play("OverTheLine");
			i.get_node("Sprite/Light2D").set_enabled(false);
			i.Stop = true;
		Stop = true

func _on_Anim_finished():
	get_parent().get_node("Score").add_score(100);
	get_parent().get_node("Camera").shake(1.0);
	queue_free();
