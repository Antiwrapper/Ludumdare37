extends RigidBody2D

var Explode = false;

func _integrate_forces(state):
	if (state.get_contact_count() == 1):
		var body = state.get_contact_collider_object(0);
		if (body != null):
			if (Explode == true && body.has_method("kill")):
				body.kill();
			elif (body.has_method("hit")):
				body.hit();
		if (Explode == true):
			get_parent().get_node("Sound").Play("Explosion");
			var e = preload("res://Scenes/Explosion.tscn").instance();
			get_parent().add_child(e);
			e.set_pos(get_pos() - (get_linear_velocity().normalized() * 8));
			e.get_node("Particles").set_color(get_node("Sprite").get_modulate());
			e.get_node("Particles").set_emitting(true);
		queue_free();