extends Camera2D

var ShakeTimer = 0.0;

func _ready():
	set_process(true);

func _process(delta):
	if (ShakeTimer > 0):
		ShakeTimer -= delta;
		set_offset(Vector2(rand_range(-16, 16), rand_range(-16, 16)));
	else:
		set_offset(Vector2(0, 0));

func shake(time):
	ShakeTimer = time;
	get_parent().get_node("Sound").Play("Shake");