extends KinematicBody2D

const SPEED = 400;
const UP_SPEED = 200;
const ACCELERATION = 100;

var Velocity = Vector2();
var Attack = false;
var BulletSpeed = 400;

func _ready():
	set_fixed_process(true);
	add_to_group("Enemies");

func _fixed_process(delta):
	if ((get_parent().get_node("Player").get_pos() - get_pos()).length() > 720 && Attack == false):
		Attack = true;
	elif (Attack == true && (get_parent().get_node("Player").get_pos() - get_pos()).length() > 400):
		var top_speed = (get_parent().get_node("Player").get_pos() - get_pos()).normalized() * SPEED;
		if (Velocity.x < top_speed.x):
			Velocity.x += ACCELERATION * delta * 4;
		elif (Velocity.x > top_speed.x):
			Velocity.x -= ACCELERATION * delta * 4;
		
		if (Velocity.y < top_speed.y):
			Velocity.y += ACCELERATION * delta * 4;
		elif (Velocity.y > top_speed.y):
			Velocity.y -= ACCELERATION * delta * 4;
	else:
		if (Attack == true):
			Attack = false;
			_shoot();
		if (Velocity.y > -UP_SPEED):
			Velocity.y -= ACCELERATION * delta * 3;
		if (Velocity.x > 100):
			Velocity.x -= ACCELERATION * delta;
		elif (Velocity.x < -100):
			Velocity.x += ACCELERATION * delta;
		if (get_pos().y < 32):
			Attack = true;
	
	var motion = move(Velocity * delta);
	if (is_colliding() == true):
		var n = get_collision_normal();
		motion = n.slide(motion);
		Velocity = n.slide(Velocity);
		move(motion);

func hit():
	get_parent().get_node("Score").add_score(100);
	get_parent().get_node("Sound").Play("Hit");
	queue_free();

func _shoot():
	var b = preload("res://Scenes/Bullet.tscn").instance();
	get_parent().add_child(b);
	b.add_collision_exception_with(self);
	b.set_pos(get_pos());
	b.get_node("Sprite").set_modulate(Color("8d4f4f"));
	var player_pos = get_parent().get_node("Player").get_pos();
	var rand = rand_range(0.8, 1.2);
	b.set_linear_velocity(((player_pos * rand) - b.get_pos()).normalized() * BulletSpeed); 