extends Node;

var current_scene = null;
var Music = StreamPlayer.new();
var SoundMuted = false;
var Pressed = false;
var Fullscreen = false;

func _ready():
	set_process(true);
	var music = preload("res://Resources/Audio/Music.ogg");
	Music.set_stream(music);
	Music.set_loop(true);
	add_child(Music);
	Music.play();
	
	randomize();
	current_scene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1);

func _process(delta):
	if (Input.is_action_pressed("Tutorial")):
		goto_scene("res://Scenes/Tutorial.tscn");
	elif (Input.is_action_pressed("Escape")):
		goto_scene("res://Scenes/Menu.tscn");
	elif (Input.is_action_pressed("Fullscreen") && Pressed == false):
		Pressed = true;
		if (Fullscreen == false):
			OS.set_window_fullscreen(true);
			Fullscreen = true;
		else:
			Fullscreen = false;
			OS.set_window_fullscreen(false);
		
	if (Input.is_action_pressed("MuteS") && Pressed == false):
		Pressed = true;
		if (SoundMuted == false):
			SoundMuted = true;
		else:
			SoundMuted = false;
	if (Input.is_action_pressed("MuteM") && Pressed == false):
		Pressed = true;
		if (Music.is_paused() == false):
			Music.set_paused(true);
		else:
			Music.set_paused(false);
	if (Input.is_action_pressed("MuteM") == false && Input.is_action_pressed("MuteS") == false && Input.is_action_pressed("Fullscreen") == false):
		Pressed = false;

func goto_scene(path):
	call_deferred("_goto_scene", path);

func _goto_scene(path):
	current_scene.free();
	
	current_scene = ResourceLoader.load(path).instance();
	
	get_tree().get_root().add_child(current_scene);
	get_tree().set_current_scene(current_scene);

func restart():
	goto_scene("res://Scenes/Root.tscn");