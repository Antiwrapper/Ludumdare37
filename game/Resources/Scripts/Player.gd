extends KinematicBody2D

const GRAVITY = 982;
const SPEED = 500;
const COOLDOWN = 0.5;
const FLOOR_THRESHOLD = 50;
const JUMP = 400;
const JUMP_DURATION = 0.8;

var JumpTime = JUMP_DURATION;
var Ground = false;
var Cooldown = COOLDOWN;
var Velocity = Vector2();
var Stop = false;


func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	Stop = true;
	Velocity.y += GRAVITY * delta;
	if (Cooldown >= 0):
		Cooldown -= delta
	
	if (Input.is_action_pressed("Right") == true):
		Velocity.x = SPEED;
		Stop = false;
	if (Input.is_action_pressed("Left") == true):
		Velocity.x = -SPEED;
		Stop = false;
	
	if (Stop == true):
		Velocity.x = 0;
	
	if (Input.is_action_pressed("Jump") == true && Ground == true):
		Velocity.y = -JUMP;
	elif (Input.is_action_pressed("Jump") == true && JumpTime > 0):
		JumpTime -= delta;
		Velocity.y -= GRAVITY/2 * delta;
	
	if (Input.is_action_pressed("Shoot") == true && Cooldown < 0):
		Cooldown = COOLDOWN;
		_shoot();
	
	var motion = move(Velocity * delta);
	var floor_velocity = Vector2();
	
	if (is_colliding() == true):
		var n = get_collision_normal();
		if (rad2deg(acos(n.dot(Vector2(0, -1)))) < FLOOR_THRESHOLD):
			floor_velocity = get_collider_velocity();
			JumpTime = JUMP_DURATION;
			Ground = true;
		motion = n.slide(motion);
		Velocity = n.slide(Velocity);
		move(motion);
	
	if (floor_velocity != Vector2()):
		move(floor_velocity * delta);
	
	if (Ground == true && Velocity.y < 0):
		Ground = false;

func _shoot():
	var b = preload("res://Scenes/Bullet.tscn").instance();
	get_parent().add_child(b);
	b.set_pos(get_pos());
	b.Dir = (get_global_mouse_pos() - b.get_pos()).normalized();