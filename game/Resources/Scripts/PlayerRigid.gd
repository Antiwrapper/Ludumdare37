extends RigidBody2D

const JUMP_HEIGHT = 350;
const JUMP_DURATION = 0.3;
const SPEED = 400;
const FLOOR_THRESHOLD = 45;
const COOLDOWN = 0.5;

var Move = 0;
var BonusSpeed = 0;
var Jump = false;
var AirTime = 0;
var Cooldown = COOLDOWN;
var BulletSpeed = 1000;
var Death = false;
var Health = 3;
var Invincible = 0.0;
var Ammo = 0;
var SpeedTimer = 0;
var Weapon = "Standard";
var Blast = 0;

func _ready():
	set_process(true);

func _process(delta):
	if (Death == false):
		if (SpeedTimer > 0):
			SpeedTimer -= delta;
			if (SpeedTimer <= 0):
				BonusSpeed = 0;
				get_node("Speed").set_emitting(false);
				get_parent().get_node("Score/PowerUp Activated").set_hidden(true);
		if (Invincible > 0):
			Invincible -= delta;
		if (Cooldown >= 0):
			Cooldown -= delta;
	
		AirTime += delta;
		if (get_pos().x > 1280 || get_pos().x < 0 || get_pos().y < -1300 || get_pos().y > 720):
			hit();
	
		if (get_linear_velocity().y < 0.1 && get_linear_velocity().y > -0.1):
			AirTime = 0;
		Move = 0;
		if (Input.is_action_pressed("Right") == true):
			Move += 1;
		if (Input.is_action_pressed("Left") == true):
			Move -= 1;
		if (Input.is_action_pressed("Jump") == true && Jump == false && AirTime < JUMP_DURATION):
			Jump = true;
		if (Input.is_action_pressed("Blast") == true && Blast > 0):
			get_parent().get_node("Sound").Play("Explosion");
			Blast -= 1;
			if (Blast == 0):
				get_parent().get_node("Score/PowerUp Activated").set_hidden(true);
			var blocks = get_tree().get_nodes_in_group("Blocks");
			for i in blocks:
				i.get_node("Anim").play("OverTheLine");
				i.get_node("Sprite/Light2D").set_enabled(false);
				i.apply_impulse(i.get_pos(), Vector2(0, -700));
				i.Stop = true;
	
		if (Input.is_action_pressed("Shoot") == true && Cooldown < 0):
			if (Weapon != "Standard" && Ammo > 0):
				if (Weapon == "Machine gun"):
					Cooldown = 0.1;
				else:
					Cooldown = COOLDOWN;
				get_parent().get_node("Sound").Play("Shoot");
				_shoot();
				Ammo -= 1;
				if (Ammo == 0):
					Weapon = "Standard";
					get_parent().get_node("Score/PowerUp Activated").set_hidden(true);
			elif (Weapon == "Standard"):
				Cooldown = COOLDOWN;
				get_parent().get_node("Sound").Play("Shoot");
				_shoot();

func _integrate_forces(state):
	#set_friction(0);
	for i in range(0, state.get_contact_count()):
		var n = state.get_contact_local_normal(i);
		if (rad2deg(acos(n.dot(Vector2(0, -1)))) < FLOOR_THRESHOLD):
			AirTime = 0;
		#if (n.x >= 0.9):
		#	set_friction(0);
		#elif (n.x <= -0.9):
		#	set_friction(0);
	
	if (Move == 1):
		set_linear_velocity(Vector2(SPEED + BonusSpeed, get_linear_velocity().y));
	elif (Move == -1):
		set_linear_velocity(Vector2(-SPEED - BonusSpeed, get_linear_velocity().y));
	elif (Move == 0):
		set_linear_velocity(Vector2(get_linear_velocity().x / 2, get_linear_velocity().y));
	
	if (Jump == true):
		Jump = false;
		set_axis_velocity(Vector2(0, -JUMP_HEIGHT));
	
	if (Input.is_action_pressed("Restart") == true):
		get_node("/root/Global").restart();

func hit():
	if (Invincible <= 0.0):
		Health -= 1;
		if (Health == 2):
			get_node("Sprite").set_modulate(Color("ffd149"));
			get_parent().get_node("FloorBody").get_node("Floor sprite left").set_modulate(Color("ffd149"));
			get_parent().get_node("FloorBody").get_node("Floor sprite right").set_modulate(Color("ffd149"));
			get_parent().get_node("Walls/Left wall sprite").set_modulate(Color("ffd149"));
			get_parent().get_node("Walls/Left wall sprite up").set_modulate(Color("ffd149"));
			get_parent().get_node("Walls/Right wall sprite").set_modulate(Color("ffd149"));
			get_parent().get_node("Walls/Right wall sprite up").set_modulate(Color("ffd149"));
		elif (Health == 1):
			get_node("Sprite").set_modulate(Color("aa3131"));
			get_parent().get_node("FloorBody").get_node("Floor sprite left").set_modulate(Color("aa3131"));
			get_parent().get_node("FloorBody").get_node("Floor sprite right").set_modulate(Color("aa3131"));
			get_parent().get_node("Walls/Left wall sprite").set_modulate(Color("aa3131"));
			get_parent().get_node("Walls/Left wall sprite up").set_modulate(Color("aa3131"));
			get_parent().get_node("Walls/Right wall sprite").set_modulate(Color("aa3131"));
			get_parent().get_node("Walls/Right wall sprite up").set_modulate(Color("aa3131"));
		elif (Health == 0 && Death == false):
			get_parent().get_node("Camera").shake(0.6);
			get_node("Anim").play("Death");
			set_linear_velocity(Vector2(0, 0));
			set_gravity_scale(0);
			Move = 0;
			get_node("Speed").set_emitting(false);
			Death = true;
		
		if (Death == false):
			get_parent().get_node("Camera").shake(0.6);
			get_node("Anim").play("Blinking");
			Invincible = 2.0;

func hit_by_walking():
	hit();

func _shoot():
	var Dir = (get_global_mouse_pos() - get_pos()).normalized();
	
	var b = preload("res://Scenes/Bullet.tscn").instance();
	var b1 = preload("res://Scenes/Bullet.tscn").instance();
	var b2 = preload("res://Scenes/Bullet.tscn").instance();
	
	if (Weapon == "Shotgun"):
		get_parent().add_child(b1);
		get_parent().add_child(b2);
		
		b1.add_collision_exception_with(self);
		b1.add_collision_exception_with(b2);
		b1.add_collision_exception_with(b);
		b1.set_collision_mask_bit(0, 1);
		b1.set_collision_mask_bit(3, 1);
		b1.set_pos(get_pos());
		b1.get_node("Sprite").set_modulate(get_node("Sprite").get_modulate());
		
		b2.add_collision_exception_with(self);
		b2.add_collision_exception_with(b1);
		b2.add_collision_exception_with(b);
		b2.set_collision_mask_bit(0, 1);
		b2.set_collision_mask_bit(3, 1);
		b2.set_pos(get_pos());
		b2.get_node("Sprite").set_modulate(get_node("Sprite").get_modulate());
		
		var angle = -atan2(Dir.x, Dir.y) + (PI / 2);
		var Dir1 = Vector2(cos(angle + 0.3), sin(angle + 0.3)).normalized();
		
		b1.set_linear_velocity(Dir1 * BulletSpeed);
		
		Dir1 = Vector2(cos(angle - 0.3), sin(angle - 0.3)).normalized();
		
		b2.set_linear_velocity(Dir1 * BulletSpeed); 
	
	get_parent().add_child(b);
	b.add_collision_exception_with(self);
	b.add_collision_exception_with(b1);
	b.add_collision_exception_with(b2);
	b.set_collision_mask_bit(0, 1);
	b.set_collision_mask_bit(3, 1);
	b.set_pos(get_pos());
	b.get_node("Sprite").set_modulate(get_node("Sprite").get_modulate());
	b.set_linear_velocity(Dir * BulletSpeed); 
	if (Weapon == "Rocket launcher"):
		b.set_mass(5);
		b.Explode = true;
	elif (Weapon != "Shotgun"):
		b1.free();
		b2.free();