extends KinematicBody2D

const LOWEST_Y = 300;
const ACCELERATION = 150;
const MAX_SPEED = 300;

var Dir = Vector2();
var Velocity = Vector2();
var Disappear = false;
var Sort = 0;
var Weapon = 0;

func _ready():
	Dir = (Vector2(rand_range(32, 1248), 600) - get_pos()).normalized();
	Sort = round(rand_range(-0.4, 2.49)); # 2 = Weapon, 1 = Speed, 0 = Blast
	if (Sort == 2):
		get_node("Label").set_text("W");
		Weapon = round(rand_range(-0.49, 2.49)); # 0 = Machine gun, 1 = Shotgun, 2 = Rocket launcher
	elif (Sort == 1):
		get_node("Label").set_text("S");
	elif (Sort == 0):
		get_node("Label").set_text("B");
	set_fixed_process(true);

func _fixed_process(delta):
	if (get_pos().y < LOWEST_Y && Disappear == false):
		if (Velocity.length() < MAX_SPEED):
			var top_speed = Dir * MAX_SPEED;
			
			if (Velocity.x < top_speed.x):
				Velocity.x += ACCELERATION * delta;
			elif (Velocity.x > top_speed.x):
				Velocity.x -= ACCELERATION * delta;
			
			if (Velocity.y < top_speed.y):
				Velocity.y += ACCELERATION * delta;
			elif (Velocity.y > top_speed.y):
				Velocity.y -= ACCELERATION * delta;
	else:
		Disappear = true;
		if (Velocity.y > -MAX_SPEED):
			Velocity.y -= ACCELERATION * delta;
	if (Disappear == true && get_pos().y < -32):
		queue_free();
	move(Dir * Velocity * delta);

func hit():
	get_parent().get_node("Sound").Play("Powerup");
	if (Sort == 1):
		get_parent().get_node("Player").BonusSpeed += 300;
		get_parent().get_node("Player").SpeedTimer = 10;
		get_parent().get_node("Player/Speed").set_color(get_parent().get_node("Player/Sprite").get_modulate());
		get_parent().get_node("Player/Speed").set_emitting(true);
		get_parent().get_node("Score/PowerUp Activated").set_hidden(false);
		get_parent().get_node("Score/PowerUp Activated").set_text("Speedboost!!!");
	elif (Sort == 2):
		if (Weapon == 0):
			get_parent().get_node("Player").Weapon = "Machine gun";
			get_parent().get_node("Player").Ammo += 90;
			get_parent().get_node("Score/PowerUp Activated").set_hidden(false);
			get_parent().get_node("Score/PowerUp Activated").set_text("Machine gun!!!");
		elif (Weapon == 1):
			get_parent().get_node("Player").Weapon = "Shotgun";
			get_parent().get_node("Player").Ammo += 30;
			get_parent().get_node("Score/PowerUp Activated").set_hidden(false);
			get_parent().get_node("Score/PowerUp Activated").set_text("Shotgun!!!");
		elif (Weapon == 2):
			get_parent().get_node("Player").Weapon = "Rocket launcher";
			get_parent().get_node("Player").Ammo += 10;
			get_parent().get_node("Score/PowerUp Activated").set_hidden(false);
			get_parent().get_node("Score/PowerUp Activated").set_text("Rocket launcher!!!");
	elif (Sort == 0):
		get_parent().get_node("Player").Blast += 1;
		get_parent().get_node("Score/PowerUp Activated").set_hidden(false);
		get_parent().get_node("Score/PowerUp Activated").set_text("Press space for a blast!!!");
	queue_free();