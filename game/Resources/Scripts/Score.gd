extends Control

var Score = 0;
var Stop = false;

func _ready():
	set_process(true);

func _process(delta):
	if (has_node("ScoreText") == true):
		if (get_parent().get_node("Player").Death == true && Stop == false):
			get_node("ScoreText").set_hidden(true);
			get_node("DeadMessage").set_hidden(false);
			var text = get_node("DeadMessage").get_text();
			get_node("DeadMessage").set_text(text + " " + var2str(Score));
			Stop = true;
	else:
		get_parent().get_node("Spawner").Menu = true;
		if (Input.is_action_pressed("Blast") == true):
			get_node("/root/Global").goto_scene("res://Scenes/Root.tscn");

func add_score(score):
	if (has_node("ScoreText") == true):
		if (get_parent().get_node("Player").Death == false):
			Score += score;
		get_node("ScoreText").set_text("Score: " + var2str(Score));