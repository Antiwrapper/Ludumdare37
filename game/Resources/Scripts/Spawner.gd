extends Node2D

var BlockTimer = 0;
var EnemyTimer = 0;
var PowerTimer = 0;
var GameTime = 0;
var Menu = false;

func _ready():
	BlockTimer = rand_range(0, 2);
	EnemyTimer = rand_range(5, 10);
	PowerTimer = rand_range(10, 20);
	set_fixed_process(true);

func _fixed_process(delta):
	BlockTimer -= delta;
	EnemyTimer -= delta;
	PowerTimer -= delta;
	if (BlockTimer < 0):
		BlockTimer = rand_range(2, 4);
		var b = preload("res://Scenes/Block.tscn").instance();
		b.set_pos(Vector2(rand_range(160.0, 1120), get_pos().y));
		get_parent().add_child(b);
	
	if (EnemyTimer < 0 && Menu == false):
		if (get_parent().get_node("Score").Score > 3000):
			EnemyTimer = rand_range(2, 4);
		else:
			EnemyTimer = rand_range(4, 8);
		var enemy;
		var type = rand_range(0, 2);
		var e;
		if (type < 1):
			e = preload("res://Scenes/FlyingEnemy.tscn").instance();
		else:
			e = preload("res://Scenes/WalkingEnemy.tscn").instance();
		e.set_pos(Vector2(rand_range(160.0, 1120), -64));
		get_parent().add_child(e);
	
	if (PowerTimer < 0 && Menu == false):
		PowerTimer = rand_range(8, 18);
		var p = preload("res://Scenes/Powerup.tscn").instance();
		p.set_pos(get_pos());
		get_parent().add_child(p);