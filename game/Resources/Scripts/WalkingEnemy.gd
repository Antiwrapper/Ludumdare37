extends RigidBody2D

const WALKING_SPEED = 300;
const JUMPING_HEIGHT = 300;
const JUMPING_TIME_MAX = 5;
const JUMPING_TIME_MIN = 3;
const SHOOTING_TIME_MAX = 4;
const SHOOTING_TIME_MIN = 2;

var Shooter = false;
var Health = 3;
var Dir = 0.2;
var JumpTimer = JUMPING_TIME_MAX;
var ShootTimer = 3;
var Jump = false;

func _ready():
	var choose = round(rand_range(0, 1.1));
	if (choose == 1):
		Shooter = false;
	else:
		Shooter = true;
		get_node("Sprite").set_modulate(Color("8d4f4f"));
	set_process(true);
	add_to_group("Enemies");

func _process(delta):
	JumpTimer -= delta;
	if (Shooter == true):
		ShootTimer -= delta;
		if (ShootTimer < 0):
			ShootTimer = rand_range(SHOOTING_TIME_MIN, SHOOTING_TIME_MAX);
			_shoot();
	if (JumpTimer < 0):
		JumpTimer = rand_range(JUMPING_TIME_MIN, JUMPING_TIME_MAX);
		Jump = true;
	if (get_pos().x > 1280 || get_pos().x < 0):
		queue_free();

func hit():
	get_parent().get_node("Sound").Play("Hit");
	Health -= 1;
	if (Health == 0):
		get_parent().get_node("Score").add_score(100);
		queue_free();

func kill():
	hit();
	hit();
	hit();

func _shoot():
	var b = preload("res://Scenes/Bullet.tscn").instance();
	get_parent().add_child(b);
	b.add_collision_exception_with(self);
	b.set_pos(get_pos());
	b.get_node("Sprite").set_modulate(Color("8d4f4f"));
	if (Dir > 0):
		b.set_linear_velocity(Vector2(400, 0));
	else:
		b.set_linear_velocity(Vector2(-400, 0));

func _integrate_forces(state):
	set_friction(0);
	for i in range(0, state.get_contact_count()):
		var body = state.get_contact_collider_object(i);
		if (body != null):
			if (body.has_method("hit_by_walking")):
				body.hit_by_walking();
		var n = state.get_contact_local_normal(i);
		if (n.x >= 0.9):
			Dir = 1;
			get_node("Sprite").set_scale(Vector2(0.5, 0.75));
		elif (n.x <= -0.9):
			Dir = -1;
			get_node("Sprite").set_scale(Vector2(-0.5, 0.75));
	if (Jump == true && get_linear_velocity().y == 0):
		Jump = false;
		set_axis_velocity(Vector2(0, -JUMPING_HEIGHT));
	
	set_linear_velocity(Vector2(Dir * WALKING_SPEED, get_linear_velocity().y));